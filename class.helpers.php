<?php
class helpers{
    
    /**
     * Ordem natural dos meses
     * @var type 
     */
    public $meses = [
        'janeiro'   => 1,  
        'fevereiro' => 2, 
        'marco'     => 3,
        'abril'     => 4,
        'maio'      => 5,
        'junho'     => 6,
        'julho'     => 7,
        'agosto'    => 8,
        'setembro'  => 9,
        'outubro'   => 10,
        'novembro'  => 11,
        'dezembro'  => 12
    ];
    
    /**
     * Indice da lista
     * @var type 
     */
    public $index = [
        ['Mês', 'Categoria', 'Produto', 'Quantidade'],
        ['-', '-', '-', '-']
    ];
    
    private $corretor = [
        'Sabao em po'        => 'Sabão em pó',
        'Escova de dente'    => 'Escova de dentes',
        'Papel Hignico'      => 'Papel higiênico',
        'Enxaguante bocal'   => 'Enxaguante bucal',
        'Brocolis'           => 'Brócolis',
        'Geléria de morango' => 'Geléia de morango',
        'Chocolate ao leit'  => 'Chocolate ao leite'
    ];


    /**
     * Corrige as palavras erradas
     * @param type $palavra
     */
    public function corretor($palavra){
        if(!array_key_exists($palavra, $this->corretor)){
            return $palavra;
        }
        
        return $this->corretor[$palavra];
    }
}
