<?php
class db{
    private $conn;
    private $host     = 'localhost';
    private $database = 'hogwarts';
    private $user     = 'root';
    private $pass     = '1234';

    
    public function __construct(){
        $this->connect();
    }


    private function connect(){
        try {
            $this->conn = new PDO('mysql:host='.$this->host.';dbname='.$this->database, $this->user, $this->pass);
 
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }
    
    
    
    public function insert($table, $values){
        try{
            $stmt = $this->conn->prepare('INSERT INTO '.$table.' ('.implode(',', array_keys($values)).') VALUES(:'.implode(', :', array_keys($values)).')');

            foreach($values as $key=>$value){
                $newArray[':'.$key] = $value;
            }

            $stmt->execute($newArray);
        }
        catch(Exception $e){
            die($e->getMessage());
        }
    }
}

