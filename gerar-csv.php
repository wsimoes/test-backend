<?php
require_once 'class.helpers.php';

$help = new helpers();

//cria csv para armazenar a nova lista
$csv = fopen('compras-do-ano.csv', 'w');

//abre a listinha de papel do Dobby
$lista_antiga = include('lista-de-compras.php');

//grava o indice no arquivo
foreach ($help->index as $linha) {
    fputcsv($csv, $linha);
}

//percorre e ordena a lista do Dobby
foreach ($lista_antiga as $mes => $categorias) {
    ksort($categorias);
    foreach ($categorias as $categoria => $produtos){
        arsort($produtos);
        if(count($produtos) > 0){
            foreach ($produtos as $produto => $quantidade){
                $linhas[$help->meses[$mes]][] = [$mes, $categoria, $help->corretor($produto), $quantidade];
            }
        }
    }    
}

//faz a ordenação natural dos meses
ksort($linhas);

//grava a nova lista no csv
foreach($linhas as $mes_ordem => $index){
    foreach($index as $linha){
        fputcsv($csv, $linha);
    }
}


fclose($csv);
