<?php
require_once 'class.db.php';
require_once 'class.helpers.php';

$help = new helpers();
$db   = new db();

//abre a listinha de papel do Dobby
$lista_antiga = include('lista-de-compras.php');


//percorre e ordena a lista do Dobby
foreach ($lista_antiga as $mes => $categorias) {
    ksort($categorias);
    foreach ($categorias as $categoria => $produtos){
        arsort($produtos);
        if(count($produtos) > 0){
            foreach ($produtos as $produto => $quantidade){
                
                $db->insert('lista_compras', [
                    'mes'        => $mes, 
                    'categoria'  => $categoria, 
                    'produto'    => $help->corretor($produto), 
                    'quantidade' => (int) $quantidade]);
            }
        }
    }    
}


